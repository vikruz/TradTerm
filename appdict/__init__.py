import os
import django

os.environ['DJANGO_SETTINGS_MODULE'] = 'appdict.settings'
django.setup()
