"""appdict URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
# pylint: disable=C0103
from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
#from django.contrib import admin
from features.dicterm import views
from features.wikilenguaje.views import lenguaje_detallado
from django.conf import settings
from django.conf.urls.static import static
from appdict import views
from features.dicterm.admin import admin_site
urlpatterns = [
    url(r'^logout/$', auth_views.logout, name='logout'),
    url(r'^login/$', views.Autenticar_usuario.as_view(), name='login'),
    url(r'^admin/', admin_site.urls),
    url(r'^lenguajes/', include('features.wikilenguaje.urls', namespace='Lenguajes')),
    url(r'^lenguajes/(?P<nombre_lenguaje>[^\.]+)', lenguaje_detallado, name='lenguaje_detallado'),
    url(r'^terminos/', include('features.dicterm.urls', namespace='Terminos')),
    url(r'^$', views.index, name='index')
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
