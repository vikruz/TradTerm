"""El modelo lenguaje contiene toda la informacion del lenguanje
 al que se hace referencia en los terminos."""
from __future__ import unicode_literals

from django.db import models

""" Un lenguaje contiene estos atributos
 nombre, descripcion, caracteristicas."""
from django.core.validators import URLValidator

# Create your models here.

class Lenguaje(models.Model):
    """
    En este modelo guarda un lenguaje con todos sus atributos
    """
    nombre = models.CharField(max_length=50, unique=True)
    descripcion = models.TextField()
    caracteristicas = models.TextField(default='')
    imagenext = models.TextField(validators=[URLValidator()] ,default='https://cdn4.iconfinder.com/data/icons/smiley-vol-3-2/48/134-512.png')
    imagen = models.ImageField(
        upload_to='logos/lenguajes/', default='logos/no-img.jpg')

    def __str__(self):
            return '%s' % (self.nombre)
