"""
    En estas vistas defino directamente todo el funcionamiento del feature
    wikilenguajes.
    falta agregar mas funcionalidades.
"""

from django.contrib.auth.decorators import login_required

from django.shortcuts import render, redirect, HttpResponse, get_object_or_404

from features.wikilenguaje.forms import LenguajeForm

from features.wikilenguaje.models import Lenguaje

from django.db.models import Q

from django.http import JsonResponse

from django.core import serializers
import json
# pylint: disable=E1101
# Create your views here.


def index_lenguajes(request):
    """Esta funcion renderiza la lista de lenguajes como objetos."""
    lenguaje = Lenguaje.objects.all().order_by('nombre')
    contexto = {'lenguajes':lenguaje}
    return render(request, 'wikilenguaje/index.html', contexto)

def lenguaje_detallado(request, nombre_lenguaje):
    """Esta funcion muestra la informacion contenida dentro del lenguaje."""
    lenguaje = get_object_or_404(Lenguaje, nombre=nombre_lenguaje)
    print ("El parametro recibido es: " + nombre_lenguaje)
    contexto = {'lenguaje':lenguaje}
    return render(request, 'wikilenguaje/lenguaje_detallado.html', contexto)

@login_required
def lenguaje_view(request):
    """Funcion que hace posible el metodo post del formulario lenguaje nuevo."""
    if request.method == 'POST':
        form = LenguajeForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('Lenguajes:index_lenguajes')
    else:
        form = LenguajeForm()
    return render(request, 'wikilenguaje/lenguaje_form.html', {'form':form})

def buscarl(request):
    a = request.GET.get('query','')
    querys = (Q(nombre__icontains=a) | Q(descripcion__icontains=a))
    querys |= Q(caracteristicas__icontains=a)
    res = Lenguaje.objects.filter(querys)
    contexto = {'lenguajes': res}
    return render(request, 'wikilenguaje/index.html', contexto)
