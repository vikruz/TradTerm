from django import forms

from features.wikilenguaje.models import Lenguaje

class LenguajeForm(forms.ModelForm):

    class Meta:
        model = Lenguaje

        fields = [
            'nombre',
            'descripcion',
            'caracteristicas'
        ]
        labels = {
            'nombre': 'Nombre',
            'descripcion': 'Descripcion',
            'caracteristicas': 'Caracteristicas'
        }
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'descripcion': forms.Textarea(attrs={'class':'form-control'}),
            'caracteristicas': forms.Textarea(attrs={'class':'form-control'}),
        }
