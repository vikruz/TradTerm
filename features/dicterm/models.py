""" Un termino contiene estos atributos
 nombre , significado , descripcion y Un lenguaje al que pertenece.
 la relacion que encaja en este modelo de datos es mochos terminos pertenece a un lenguaje."""
from __future__ import unicode_literals

from django.db import models

from features.wikilenguaje.models import Lenguaje


# Create your models here.

class Termino(models.Model):
    """
    Guarda en la base de datos un termino con sus atributos correspondientes
    """
    nombre = models.CharField(max_length=30)
    significado = models.CharField(max_length=30)
    descripcion = models.TextField()
    lenguaje = models.ForeignKey(Lenguaje, blank=True, null=True, on_delete=models.CASCADE)

    def __str__(self):
            return 'Termino: ' '%s ''Traduccion:'' %s' % (self.nombre, self.significado)
