from django.contrib.admin import AdminSite

from django.contrib import admin

from django.contrib.auth.models import Group, User

from django.db import models

from .models import Termino, Lenguaje

class MyAdminSite(AdminSite):
    site_header = 'TradTerm Administracion'
    site_title = 'Tradterm Admin Title'
    index_title = 'Tradterm Admin INDEX Title'
admin_site = MyAdminSite(name='myadmin')

class TerminoModelAdmin(admin.ModelAdmin):
	fields = ('nombre', 'significado', 'descripcion', 'lenguaje' )
	list_display = ('nombre', 'significado', 'descripcion', 'lenguaje')
	list_display_links = ['nombre']
	list_editable = ['lenguaje']
	list_filter = ['lenguaje']
	search_fields = ['significado','descripcion']
	class Meta:
		model = Termino

class LenguajeModelAdmin(admin.ModelAdmin):
	list_display = ('nombre','imagenext','imagen')
	list_display_links = ['nombre']
	search_fields = ['nombre','descripcion', 'caracteristicas']
	class Meta:
		model = Lenguaje

admin_site.register(Group)
admin_site.register(User)
admin_site.register(Termino, TerminoModelAdmin)
admin_site.register(Lenguaje, LenguajeModelAdmin)
