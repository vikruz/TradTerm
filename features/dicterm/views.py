"""
    Aqui defino solo el indexador de terminos 
    el indexador de terminos esta vendria siendo la idea principal.
"""
from django.shortcuts import render

from features.dicterm.models import Termino

# Create your views here.
# pylint: disable=E1101

def index_terminos(request):
    """Renderizo  el template que hace posible la vista de terminos."""
    termino = Termino.objects.all()
    contexto = {'terminos':termino}
    return render(request, 'dicterm/index.html', contexto)
